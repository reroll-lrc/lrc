import api from '@/services/api'

export default {

    getAdministrate(managerId) {
        return api().get(`/administrate/${managerId}`)
    },

    newAdministrate(ids) {
        return api().post('/administrate',ids)
    },

    deleteAdministrate(managerId, teamId) {
        return api().delete(`/manager/${managerId}/team/${teamId}`)
    }
}