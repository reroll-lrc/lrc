import api from '@/services/api'

export default {
    getManagers() {
        return api().get('managers')
    },
    getManager(managerId) {
        return api().get(`/managers/${managerId}`)
    },
    create(manager) {
        return api().post('register',manager)
    },
    edit(managerId,manager) {
        return api().put(`/managers/${managerId}`,manager)
    },
    remove(managerId) {
        return api().delete(`/managers/${managerId}`)
    }
}