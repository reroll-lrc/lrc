import axios from 'axios'
import store from '@/store'

export default () => {
    return axios.create({
        baseURL: 'https://lrcapi.nelopsis.fr/',
        headers: {
            Authorization: `Bearer ${store.state.token}`
        }
    })
}
