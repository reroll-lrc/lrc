import api from '@/services/api'

export default {
    getAllTeams() {
        return api().get('teams')
    },
    managerTeam(manager) {
        return api().get(`manager/${manager}/team`)
    },
    create(team) {
        return api().post('register',team)
    },
    edit(team,teamId) {
        return api().put(`teams/${teamId}`,team)
    },

}