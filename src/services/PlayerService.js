import api from '@/services/api'

export default {
    getAllPlayers() {
        return api().get('players')
    },
    getPlayer(player) {
        return api().get(`players/${player}`)
    },
    create(player) {
        return api().post('players', player)
    },
    edit(player, playerId) {
        return api().put(`players/${playerId}`,player)
    }
}