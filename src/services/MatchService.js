import api from '@/services/api'

export default {
    getAllMatches() {
        return api().get('matches')
    },
    getMatch(id) {
        return api().get(`matches/${id}`)
    },
    editMatch(id, newMatch) {
        return api().put(`matches/${id}`, newMatch)
    }
}