import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/Home.vue'
import CreateTeam from '@/components/CreateTeam.vue'
import EditTeam from '@/components/EditTeam.vue'
import AdminTeam from '@/components/admin/AdminTeam.vue'
import Results from '@/components/Results.vue'
import Playoffs from '@/components/playoffs/Playoffs.vue'
import MatchEdit from '@/components/playoffs/MatchEdit.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/edit',
    name: 'EditTeam',
    component: EditTeam
  },
  {
    path: '/team/create',
    name: 'CreateTeam',
    component: CreateTeam
  },
  {
    path: '/admin',
    name: 'AdminTeam',
    component: AdminTeam
  },
  {
    path: '/results',
    name: 'Results',
    component: Results
  },
  {
    path: '/results/playoffs',
    name: 'Playoffs',
    component: Playoffs
  },
  {
    path: `/results/playoffs/match/:matchId`,
    name: 'MatchEdit',
    component: MatchEdit
  },
]

const router = new VueRouter({
  routes
})

export default router
