import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export default new Vuex.Store({
  plugins:[createPersistedState()] ,
  state: {
    token: null,
    manager:null,
    isAdmin:false,
    isUserLoggedIn: false  
  },
  mutations: {
    setToken(state,token) {
      state.token = token
      if (token) {
        state.isUserLoggedIn = true
      } else {
        state.isUserLoggedIn = false
      }
    },
    setManager(state,manager) {
      state.manager = manager
    },
    setAdmin (state,admin) {
      state.isAdmin = admin
    }
  },
  actions: {
    setToken({commit},token) {
      commit('setToken', token)
    },
    setManager({commit},manager) {
      commit('setManager',manager)
    },
    setAdmin({commit},admin) {
      commit('setAdmin',admin)
    }
  },
  modules: {
  }
})
