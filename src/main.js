import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import 'primevue/resources/themes/saga-blue/theme.css'
import 'primevue/resources/primevue.min.css' 
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css';
import firebase from 'firebase'

firebase.initializeApp({
    apiKey: process.env.firebaseApiKey,
    authDomain: process.env.firebaseAuthDomain,
    projectId: process.env.firebaseProjectId,
    storageBucket: process.env.firebaseStorageBucket,
    messagingSenderId: process.env.firebaseMessagingSenderId,
    appId: process.env.firebaseAppId,
})

import PrimeVue from 'primevue/config'

Vue.config.productionTip = false
Vue.use(PrimeVue)

sync(store,router)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
