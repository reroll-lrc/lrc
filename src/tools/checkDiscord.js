export default function isDiscordWellFormated(discord) {
    return discord.match(/\w+#\d{4}/i)
}